type ValidationDetails = [isValid: boolean, exception: Error];

export default ValidationDetails;
